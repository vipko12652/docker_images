import jenkins.model.Jenkins
import hudson.model.*

static ArrayList getJobNames(String rootDir) {
  return Jenkins.getInstance().getAllItems().findAll { it instanceof hudson.model.Job && it.getFullName() =~ rootDir}
    .collect {it.getFullName()}
}

tasksList = [:]


    script {
        String environment = env.ENVIRONMENT
        println("environment: ${environment}")

        stage("Get job name") {
            String rootJobDir = currentBuild.getFullProjectName().replaceAll('^(.*/).*', "\$1${environment}")
            //String buildName = currentBuild.getFullProjectName()
            //String rootJobDir = "JOBS"
            param = ['TAGS'   : 'deploy',
                     'HOSTS'  : 'all']
            println("rootJobDir ${rootJobDir}")
           // println("buildName ${buildName}")
            getJobNames(rootJobDir).collectEntries { j ->
                [ ("${j.replaceAll('.*/','')}".toString()): tasksList.collectEntries(j)
                    ]

            }

            println("tasksList: ${tasksList}")
            String HOSTS = env.HOSTS
            String TAGS = env.TAGS

            envList = [
                'HOSTS': HOSTS,
                'TAGS' : TAGS
            ]

            println("envList: ${envList}")
        }
    }

