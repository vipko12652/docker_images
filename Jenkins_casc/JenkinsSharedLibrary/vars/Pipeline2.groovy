import jenkins.model.Jenkins
import hudson.model.*

static ArrayList getJobNames(String rootDir) {
  return Jenkins.getInstance().getAllItems().findAll { it instanceof hudson.model.Job && it.getFullName() =~ rootDir}
    .collect {it.getFullName()}
}

String tags  = env.TAGS
String hosts = env.HOSTS

def params = [
    'HOSTS': hosts,
    'TAGS' : tags
]

def tasksList = [:]

script {
    String environment = env.ENVIRONMENT
    rootJobDir = currentBuild.getFullProjectName().replaceAll('^(.*/).*', "\$1${environment}")
    getJobNames(rootJobDir).collectEntries { j -> 
            tasksList.put(j: [param])
            println("jobs: ${j}")
    }
    println("tasksList: ${tasksList}")
}

