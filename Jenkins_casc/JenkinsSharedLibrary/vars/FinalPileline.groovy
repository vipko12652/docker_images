import jenkins.model.Jenkins
import hudson.model.*



def runJob(jobName, params) {
  def job
  try {
    job = build job: jobName, parameters: params, propagate: true
  }catch (err) {
    error "Run child job ${jobName} error"
    return false
  }
  return [num: job.getNumber(), var: job.getBuildVariables(), url: job.getAbsoluteUrl()]
}

def tasksResult = [:]  //Переменная в которую положим результаты выполнения наших пайплайнов
def tasksList = [ //Переменная со списком пайплайнов которые нужно запустить паралельно
    'ALPHA': [
    'TAGS'   : env.TAGS,
    'HOSTS'  : env.HOSTS
  ],
    'BETA': [
    'TAGS'   : env.TAGS,
    'HOSTS'  : env.HOSTS
  ],
    'EPSILON': [
    'TAGS'   : env.TAGS,
    'HOSTS'  : env.HOSTS
  ],
    'KAPPA': [
    'TAGS'   : env.TAGS,
    'HOSTS'  : env.HOSTS
  ],
    'NYU': [
    'TAGS'   : env.TAGS,
    'HOSTS'  : env.HOSTS
  ],
    'RHO': [
    'TAGS'   : env.TAGS,
    'HOSTS'  : env.HOSTS
  ]

]

pipeline {
  agent any
  stages {
    stage('Init') {
      steps {
        echo 'Init'
//        cleanWs() // Очистка проекта
//        checkout scm //Подтянуть изменения из SCM
      }
    }
    stage('Create parallel tasks') {
      steps {
        script {
          echo 'Create parallel tasks'
          def parallelTasks = [:] //Переменная которую в дальнейшем передадим функции parallel, в ней хранятся список скриптов которые надо запустить паралельно
          String environment = env.ENVIRONMENT
          rootJobDir = currentBuild.getFullProjectName().replaceAll('^(.*/).*', "\$1${environment}")
          println("rootJobDir ${rootJobDir}")
          tasksList.each { //Перебираем все таски из списка
            def taskName = it.key //Объявлять переменную обязательно, что бы передалось корректное значение
            def taskValues = it.value 
            parallelTasks["${taskName}"] = {
              script {
                def parameters = [
                  string(name: 'TAGS', value: "${taskValues.TAGS}"),
                  string(name: 'HOSTS', value: "${taskValues.HOSTS}")  //Объявляем параметры для дочернего пайплайна
                ]
                def childJob = runJob("${rootJobDir}/${taskName}", parameters) //Вызываем нашу функцию запуска пайпа и передаем туда имя и параметры
                tasksResult["${rootJobDir}/${taskName}"] = childJob.var //Записываем в taskResult результаты выполнейного пайплайна, если такие имеются
                echo "Chilb job ${taskName} build number: ${childJob.num} url: ${childJob.url}" //Просто сообщение для отладки
              }
            }
          }
          parallel parallelTasks //Запуск
        }
      }
    }
    stage('Status tasks') {
      steps {
        script {
          alpha_result = childJob.url.currentResult
          println(alpha_result) //выводим результат
        }
      }
    }
    stage('Print Result') {
      steps {
        script {
          println(tasksResult) //выводим результат
        }
      }
    }
    stage('Update gitlab status') {
      steps {
        script {
          println("update gitlabstatus") //выводим результат
        }
      }
    }
  }
}