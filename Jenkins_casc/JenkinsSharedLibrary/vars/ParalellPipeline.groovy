def runJob(jobName, params) {
  def job
  try {
    job = build job: jobName, parameters: params, propagate: true
  }catch (err) {
    error "Run child job ${jobName} error"
    return false
  }
}

static ArrayList getJobNames(String rootDir) {
  return Jenkins.getInstance().getAllItems().findAll { it instanceof hudson.model.Job && it.getFullName() =~ rootDir}
    .collect {it.getFullName()}
}

String rootDir = "Deploy/TECHNICAL_DEBT/vdavydov_test/TEST"




def tasksResult = [:]  //Переменная в которую положим результаты выполнения наших пайплайнов
def tasksList = [ //Переменная со списком пайплайнов которые нужно запустить паралельно
    'ALPHA': [
    'TAGS'   : 'deploy',
    'HOSTS'  : 'all'
  ],
    'BETA': [
    'TAGS'   : 'deploy',
    'HOSTS'  : 'all'
  ],
    'EPSILON': [
    'TAGS'   : 'deploy',
    'HOSTS'  : 'all'
  ],
]

tasksList.each { 
  def taskName = it.key
  def taskValues = it.value
  println("taskName: ${taskName}, taskValues: ${taskValues.HOSTS}")
}

pipeline {
  agent any
  stages {
    stage('Create parallel tasks') {
      steps {
        script {
          echo 'Create parallel tasks'
          String environment = env.ENVIRONMENT
          rootJobDir = currentBuild.getFullProjectName().replaceAll('^(.*/).*', "\$1${environment}")
          def parallelTasks = [:] //Переменная которую в дальнейшем передадим функции parallel, в ней хранятся список скриптов которые надо запустить паралельно
          tasksList.each { //Перебираем все таски из списка
            def taskName = it.key //Объявлять переменную обязательно, что бы передалось корректное значение
            def taskValues = it.value 
            parallelTasks["${taskName}"] = {
              script {
                def parameters = [
                  string(name: 'HOSTS', value: "${taskValues.HOSTS}"),
                  string(name: 'TAGS', value: "${taskValues.TAGS}")
                   //Объявляем параметры для дочернего пайплайна
                ]
                def childJob = runJob("${rootJobDir}/${taskName}", parameters) //Вызываем нашу функцию запуска пайпа и передаем туда имя и параметры
                tasksResult["${rootJobDir}/${taskName}"] = childJob.var //Записываем в taskResult результаты выполнейного пайплайна, если такие имеются
                echo "Chilb job ${taskName} build number: ${childJob.num} url: ${childJob.url}" //Просто сообщение для отладки
              }
            }
          }
          parallel parallelTasks //Запуск
        }
      }
    }
    stage('Print Result') {
      steps {
        script {
          println(tasksResult)
          println("False")//выводим результат
        }
      }
    }
  }
}