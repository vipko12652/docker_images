def call(Map> parameters) {
  // стандратное имя для глобального метода
  def tasks = parameters["tasks"]
  def args = parameters["args"] ?: []
 
  sh "./gradlew ${args.join(' ')}
     ${tasks.join(' ')}"
    // произвольный groovy код + pipeline-методы
}


